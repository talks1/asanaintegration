# Asana Integraetion

 This project investigated how easy it was to interact with [Asana](https://app.asana.com) functionality.

## Configuration

set the PERSONAL_ACCESS_TOKEN environment variables.

```
export PERSONAL_ACCESS_TOKEN='....'
```

## Developer API

https://developers.asana.com/docs

## How to get a Personal Access Token

https://app.asana.com/0/developer-console

