var asana = require('asana');

const debug = require('Debug')('cli')

const Dotenv = require('dotenv')

let DOTENV_CONFIG = process.env.ENVIRONMENT ? `.env.${process.env.ENVIRONMENT}` : '.env'
debug(`Environment Sourced from ${DOTENV_CONFIG}`)
Dotenv.config({path:DOTENV_CONFIG})

debug(process.env.PERSONAL_ACCESS_TOKEN)

async function Asana() {
    try {

    // replace with your personal access token. 
    var personalAccessToken = process.env.PERSONAL_ACCESS_TOKEN;

    // Construct an Asana client
    var client = asana.Client.create().useAccessToken(personalAccessToken);

    let userClient = client.users

    let me = await client.users.me()

    let workspace = me.workspaces[0].gid

    let userTaskList = await client.userTaskLists.findByUser(me.gid, {workspace})

    let tasks = await client.tasks.findByUserTaskList(userTaskList.gid)
    
    console.log(tasks.data.length)

    for(let i=0;i<tasks.data.length;i++){
        let t = tasks.data[i]
        if(t.name==='Resolve support issues'){
            if(false){
                let task = await client.tasks.findById(t.gid)
                let newComment = await client.stories.createOnTask(task.gid, {
                    text: "Set up ability to replicate data from production into local db",
                    //type: "comment"
                })
                console.log(newComment)
            }
        }
    }

    for(let i=0;i<tasks.data.length;i++){
        let t = tasks.data[i]
        console.log(t.name)
        let task = await client.tasks.findById(t.gid)

        console.log(`${task.name} ${task.completed} `)

        let stories = await client.tasks.stories(task.gid)

        for(let j=0;j<stories.data.length;j++){
            let story = stories.data[j]
            console.log(`${story.type} ${story.text}`)
        }

        if(task.parent  && task.completed){
            let parentTask = await client.tasks.findById(task.parent.gid)
            console.log(parentTask.assignee.gid)

            let p = client.tasks.update(task.gid,{assignee: parentTask.assignee.gid})
            await p
        }

    }
    
    }catch(ex){
        console.log(ex)
    }
}

Asana()
